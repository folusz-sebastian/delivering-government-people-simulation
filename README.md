# Delivering government people to different places simulation app

## Opis projektu
Aplikacja jest prostą symulacją obrazującą zajętość samochodów przewożących ważne osoby w państwie.

Osoby udają się do określonych miejsc, przy czym w każdym miejscu może znajdować się tylko jedna osoba. Osoby przebywają w określnym miejscu przez losowy czas, po czym powracają do miejsca, z którego wyruszyły.

Prezydent ma najwyższy priorytet, co prowadzi do tego, że odbierany jest z miejsca wyjazdu najwcześniej ze wszystkich oczekujących osób, a także jako pierwszy wjeżdża do miejsca, do którego się udaje.

Ekran menu aplikacji:  
![app_menu.JPG](readmeImages/app_menu.JPG)

Przykładowy ekran aplikacji:  
![app.JPG](readmeImages/app.JPG)
