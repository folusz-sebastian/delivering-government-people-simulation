package kierowcy_BOR;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Person extends Thread{
    private static int personCounter = 0;
    private static boolean premier = false;
    private static boolean prezydent = false;

    private int idP;
    private String name;

    private boolean thisPremier;
    private boolean thisPrezydent;

    private boolean finishedMeeting;

    private int allPlaces;
    private int drawnPlace;
    private Place places[];

    //grafika
    private ImageView stationImage;
    private Rectangle personMark;
    private Group personWithMark;
    private Rectangle borParkingRectangle;

    //czasy trwania
    private final int durationOfMovingPersonFromQueueToBorParking;
    private final int durationOfMovingPersonBetweenCarAndPlace;
    private final int durationOfMovingPersonFromBorParkingToStation;

    //semafory
    private Semaphore personSem;
    private Semaphore accessToPeopleSem;
    private Semaphore personArrivedToPlaceSem[];
    private Semaphore personArrivedToStationSem[];
    private Semaphore personFinishedMeetingSem[];
    private Semaphore driverInvitedToCarSem [];

    private List<Person> peopleQueue;
    private List<Group> peopleImagesQueue;

    /**
     * @param allPlaces number of all meeting places
     * @param places array of meeting place objects
     * @param peopleQueue List of person objects who waiting for cars
     * @param stationImage station image - person get out from this place
     * @param personWithMark graphic element (Group) which contains person image and frame around image
     *                       when person is Prime Minister or President
     * @param personMark graphic element (Rectangle) which is frame around image when person is Prime Minister or
     *                  President
     * @param borParkingRectangle graphic element (Rectangle) which is car (BOR) parking
     * @param peopleImagesQueue List of images in people queue (people who waiting for cars)
     */
    public Person(int allPlaces, Place[] places, Semaphore personSem, Semaphore[] personArrivedToPlaceSem,
                  Semaphore[] personArrivedToStationSem, Semaphore accessToPeopleSem,
                  Semaphore[] personFinishedMeetingSem, List<Person> peopleQueue, ImageView stationImage,
                  Group personWithMark, Rectangle personMark, Rectangle borParkingRectangle,
                  Semaphore[] driverInvitedToCarSem, List<Group> peopleImagesQueue){
        //grafika
        this.borParkingRectangle = borParkingRectangle;
        this.stationImage = stationImage;
        this.personWithMark = personWithMark;
        this.personMark = personMark;
        //pola klasy
        this.places = places;
        this.peopleImagesQueue = peopleImagesQueue;
        personCounter++;
        idP = personCounter;
        thisPremier = false;
        thisPrezydent = false;
        durationOfMovingPersonBetweenCarAndPlace = 5000;
        durationOfMovingPersonFromBorParkingToStation = 5000;
        durationOfMovingPersonFromQueueToBorParking = 5000;
        this.allPlaces = allPlaces;
        this.finishedMeeting = false;
        drawPersonType();
        //semafory
        this.personSem = personSem;
        this.accessToPeopleSem = accessToPeopleSem;
        this.personArrivedToPlaceSem = personArrivedToPlaceSem;
        this.personArrivedToStationSem = personArrivedToStationSem;
        this.personFinishedMeetingSem = personFinishedMeetingSem;
        this.peopleQueue = peopleQueue;
        this.driverInvitedToCarSem = driverInvitedToCarSem;

    }

    @Override
    public void run() {
        while (true) {
            try {
                finishedMeeting = false;
                drawnPlace = drawPlace();

                //personLeavesTheStation();

                accessToPeopleSem.acquire();
                addPersonToPeopleQueue();
                accessToPeopleSem.release();

                personSem.release();
                driverInvitedToCarSem[idP - 1].acquire();

                sleep(2000);
                accessToPeopleSem.acquire();
                removePersonImageFromPeopleQueue();
                accessToPeopleSem.release();

                personArrivedToPlaceSem[idP - 1].acquire();

                meetInPlace(drawnPlace);

                personFinishedMeetingSem[idP - 1].release();

                personArrivedToStationSem[idP - 1].acquire();
                personGoesToStation();
                stayInBase();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //                                      1.grafika - wspolrzedne

    /**
     * @return coordinate X of entry to car (BOR) parking for person
     */
    private double getEntryToBorParkingForPersonX(){
        return borParkingRectangle.getLayoutX() + 10.0;
    }

    /**
     * @return coordinate Y of entry to car (BOR) parking for person
     */
    private double getEntryToBorParkingForPersonY(){
        return borParkingRectangle.getLayoutY() - personWithMark.getBoundsInParent().getHeight();
    }

    /**
     * @return coordinate X of entry to station
     */
    private double getEntryToStationX(){
        return stationImage.getLayoutX() + stationImage.getFitWidth()/2 - personWithMark.getBoundsInParent().getWidth()/2;
    }

    /**
     * @return coordinate Y of entry station
     */
    private double getEntryToStationY(){
        return stationImage.getLayoutY() + stationImage.getFitHeight() - personWithMark.getBoundsInParent().getHeight();
    }


    //                                      2.grafika - ruch

    /**
     * method set translate transition of moving person from car (BOR) parking to station
     */
    private void movePersonFromBorParkingToStation (){
        personWithMark.setLayoutX(getEntryToBorParkingForPersonX());
        personWithMark.setLayoutY(getEntryToBorParkingForPersonY());
        System.out.println(this + " - wid(true) z bor parking do stacji");
        personWithMark.setVisible(true);

        TranslateTransition transition =
                new TranslateTransition(Duration.millis(durationOfMovingPersonFromBorParkingToStation), personWithMark);
        transition.setToX(0 - getEntryToBorParkingForPersonX() + getEntryToStationX());
        transition.setToY(0 - getEntryToBorParkingForPersonY() + getEntryToStationY());
        Platform.runLater(transition::play);

        transition.setOnFinished(event -> {
            System.out.println(this + "wid(false) na koniec z bor parking do stacji");
            personWithMark.setVisible(false);
            personWithMark.setTranslateX(0);
            personWithMark.setTranslateY(0);
            personWithMark.setLayoutX(getEntryToStationX());
            personWithMark.setLayoutY(getEntryToStationY());
        });
    }

    /**
     * method set translate transitions of:
     * -transition - moving person from getting out of the car place to entry of meeting place
     * -transition2 - waiting for end of person  meeting
     * -transition3 - moving person from entry of meeting place to  getting in the car place
     * @param indexOfPlace index Of meeting place (0-allPlaces) drawn by person
     */
    private void meetInPlace(int indexOfPlace){
        //poczatkowe ustawienia
        int durationOfMeeting = drawTimeToWait(3, 5);
        //System.out.println("durationOfMeeting: " + durationOfMeeting);

        //System.out.println(this + " - bede sie przesuwal na spotkanie");

        personWithMark.setLayoutX(places[indexOfPlace].getGetoutOfTheCarInPlaceX());
        personWithMark.setLayoutY(places[indexOfPlace].getGetoutOfTheCarInPlaceY());
        personWithMark.setVisible(true);

        //transitions
        TranslateTransition transition =
                new TranslateTransition(Duration.millis(durationOfMovingPersonBetweenCarAndPlace), personWithMark);
        transition.setToX(places[indexOfPlace].getPlaceEntryX() - places[indexOfPlace].getGetoutOfTheCarInPlaceX());
        TranslateTransition transition2 = new TranslateTransition(Duration.millis(3000), personWithMark);
        TranslateTransition transition3 =
                new TranslateTransition(Duration.millis(durationOfMovingPersonBetweenCarAndPlace), personWithMark);
        transition3.setToX(places[indexOfPlace].getGetoutOfTheCarInPlaceX() - places[indexOfPlace].getPlaceEntryX());

        transition2.setOnFinished(event -> {
            personWithMark.setTranslateX(0);
            personWithMark.setTranslateY(0);
            personWithMark.setVisible(true);
            //System.out.println(this + " - bede przesuwal na parking");
            Platform.runLater(transition3::play);
        });

        transition3.setOnFinished(event -> {
            personWithMark.setTranslateX(0);
            personWithMark.setTranslateY(0);
            personWithMark.setLayoutX(places[indexOfPlace].getGetoutOfTheCarInPlaceX());
            personWithMark.setLayoutY(places[indexOfPlace].getGetoutOfTheCarInPlaceY());
            //System.out.println(this + " - Skonczylem przesuwac na parking. Ustawiam widocznosc na true");
        });

        Platform.runLater(transition::play);

        transition.setOnFinished(event -> {
            personWithMark.setTranslateX(0);
            personWithMark.setTranslateY(0);
            personWithMark.setLayoutX(places[indexOfPlace].getPlaceEntryX());
            personWithMark.setLayoutY(places[indexOfPlace].getPlaceEntryY());
            //System.out.println(this + " - skonczylem przesuwanie na spotkanie. Widocznosc na false");
            personWithMark.setVisible(false);
            Platform.runLater(transition2::play);
        });

        try {
            sleep(2*durationOfMovingPersonBetweenCarAndPlace + durationOfMeeting + 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        finishedMeeting = true;

        //System.out.println(this + " - skonczylem meeting");
    }

    /**
     * method simulate leaving the station by person
     */
    private void personLeavesTheStation(){
        personWithMark.setLayoutX(getEntryToStationX());
        personWithMark.setLayoutY(getEntryToStationY());
        System.out.println(this + " - wid(true) - umiejscowienie w stacji");
        personWithMark.setVisible(true);
    }

    /**
     * method set translate transition of removing person image from people queue (people waiting for cars)
     */
    private void removePersonImageFromPeopleQueue (){
        peopleImagesQueue.remove(personWithMark);

        TranslateTransition transition =
                new TranslateTransition(Duration.millis(durationOfMovingPersonFromQueueToBorParking), personWithMark);

        double toX = 0 - personWithMark.getLayoutX() + getEntryToBorParkingForPersonX();
        double toY = 0 - personWithMark.getLayoutY() + getEntryToBorParkingForPersonY();

        transition.setToX(toX);
        transition.setToY(toY);
        Platform.runLater(transition::play);
        transition.setOnFinished(event -> {
            System.out.println(this + " - wid(false) - na koniec usuniecie z kolejki osoby");
            personWithMark.setVisible(false);
            personWithMark.setTranslateX(0);
            personWithMark.setTranslateY(0);
            personWithMark.setLayoutX(getEntryToBorParkingForPersonX());
            personWithMark.setLayoutY(getEntryToBorParkingForPersonY());
            for (int i = 0; i < peopleImagesQueue.size(); i++) {
                double peopleImagesQueueStartPositionX = 14;
                double peopleImagesQueueStartPositionY = 127;

                double positionX = peopleImagesQueueStartPositionX +
                        i*peopleImagesQueue.get(i).getBoundsInParent().getWidth();

                peopleImagesQueue.get(i).setLayoutX(positionX);
                peopleImagesQueue.get(i).setLayoutY(peopleImagesQueueStartPositionY);
            }
        });
    }


    //                                      3.grafika - elementy - gettery

    /**
     * @return graphic element (Group) of person with mark
     */
    public Group getPersonImage() {
        return personWithMark;
    }


    //                                      4.logika

    /**
     * @param from the lower limit of time to draw given in seconds
     * @param to the upper limit of time to draw given in seconds
     */
    private int drawTimeToWait (int from, int to){
        //zamiana na milisekundy
        from *= 1000;
        to *= 1000;

        Random random = new Random();
        return random.nextInt(to-from) + from;
    }

    /**
     * draw time to stay in station (base)
     */
    private void stayInBase(){
        try {
            sleep(drawTimeToWait(3,5)); //wylosowanie czasu pozostania w bazie od 3 do 5 sekund
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isFinishedMeeting(){
        return finishedMeeting;
    }

    public boolean isThisPremier() {
        return thisPremier;
    }

    public boolean isThisPrezydent() {
        return thisPrezydent;
    }

    public int getIdP() {
        return idP;
    }

    public int getDurationOfMovingPersonFromQueueToBorParking() {
        return durationOfMovingPersonFromQueueToBorParking;
    }

    public String toString(){
        return name;
    }

    /**
     * @param personMark graphic element (Rectangle) which is frame around image when person is Prime Minister or
     *                  President
     */
    private void markPerson (Rectangle personMark){
        personMark.setStrokeWidth(1);
    }

    /**
     * method randomly drawing person type: premier, prezez or prezydent
     */
    private void drawPersonType(){
        Random random = new Random();

        String drawPersonTab [] = {"premier", "prezes", "prezydent"};

        int i = 1;  //domyslna wartosc dla indeksu w tablicy to 2 - prezes

        if (premier && prezydent)
            name = "prezes";
        else if (premier && !prezydent){
            i = random.nextInt(2) + 1;
            name = drawPersonTab [i];
        }
        else if (!premier && prezydent){
            i = random.nextInt(2);
            name = drawPersonTab [i];
        }
        else{
            i = random.nextInt(3);
            name = drawPersonTab [i];
        }

        switch (drawPersonTab[i]) {
            case "premier":
                premier = true;
                thisPremier = true;
                markPerson(personMark);
                break;
            case "prezydent":
                prezydent = true;
                thisPrezydent = true;
                markPerson(personMark);
                break;
            default:
                name += idP;
                break;
        }
    }

    /**
     * @return index of place - integer randomly drawn from 0 to all places
     */
    private int drawPlace (){
        Random random = new Random();
        return random.nextInt(allPlaces);
    }

    public int getDrawnPlace() {
        return drawnPlace;
    }

    /**
     * method set coordinate x and coordinate y of arrangement person with mark image in people queue and
     * add person object and person with mark image to queues
     */
    private void addPersonToPeopleQueue (){
        peopleQueue.add(this);
        peopleImagesQueue.add(personWithMark);
        double peopleImagesQueueStartPositionX = 14;
        double peopleImagesQueueStartPositionY = 127;

        double positionX = peopleImagesQueueStartPositionX +
                (peopleImagesQueue.size()-1)*personWithMark.getBoundsInParent().getWidth();

        personWithMark.setLayoutX(positionX);
        personWithMark.setLayoutY(peopleImagesQueueStartPositionY);
        personWithMark.setVisible(true);
    }

    /**
     * method put thread to sleep to wait for end of moving person from car (BOR) parking to station transition
     */
    private void personGoesToStation(){
        try {
            movePersonFromBorParkingToStation();
            sleep(durationOfMovingPersonFromBorParkingToStation + 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}