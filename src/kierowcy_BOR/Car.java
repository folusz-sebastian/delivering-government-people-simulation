package kierowcy_BOR;

import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Car extends Thread{
    private static int carCounter = 0;
    private static int noOfDrivingCars;

    private int allCars;
    private int idC;
    private Person passenger;
    private Place places[];
    private double inBorParkingCarPositionX;
    private double inBorParkingCarPositionY;

    private final int durationOfMovingCarToWaitingPlaceForCars;
    private final int durationOfMovingCarToBorParking;

    //semafory
    private Semaphore personSem;
    private Semaphore accessToCarSem;
    private Semaphore accessToPlaceSem;
    private Semaphore accessToPeopleSem;
    private Semaphore placeSem[];
    private Semaphore personArrivedToPlaceSem[];
    private Semaphore personArrivedToStationSem[];
    private Semaphore personFinishedMeetingSem[];
    private Semaphore driverInvitedToCarSem[];

    //kolejki
    private Map<Integer, Person> peopleInPlaces;
    private List<Person> peopleQueue;

    //grafika
    private ImageView carImage;

    /**
     * @param places array of meeting place objects
     * @param allCars number of all cars
     * @param peopleInPlaces Map of people who are in meeting place and indexes of places
     * @param peopleQueue List of person objects who waiting for cars
     * @param carImage graphic element (ImageView) which represent car
     */
    public Car(Place[] places, int allCars, Semaphore accessToCarSem, Semaphore accessToPlaceSem, Semaphore accessToPeopleSem,
               Semaphore personSem, Semaphore[] placeSem, Semaphore[] personArrivedToPlaceSem,
               Semaphore[] personArrivedToStationSem, Semaphore[] personFinishedMeetingSem,
               Semaphore[] driverInvitedToCarSem, Map<Integer, Person> peopleInPlaces, List<Person> peopleQueue,
               ImageView carImage) {
        this.places = places;
        this.carImage = carImage;
        this.inBorParkingCarPositionX = this.carImage.getLayoutX();
        this.inBorParkingCarPositionY = this.carImage.getLayoutY();
        this.driverInvitedToCarSem = driverInvitedToCarSem;
        carCounter++;
        noOfDrivingCars = 0;
        this.idC = carCounter;
        this.allCars = allCars;
        durationOfMovingCarToWaitingPlaceForCars = 4000;
        durationOfMovingCarToBorParking = drawTimeToWait(3,5);
        //semafory
        this.personSem = personSem;
        this.accessToPeopleSem = accessToPeopleSem;
        this.accessToPlaceSem = accessToPlaceSem;
        this.placeSem = placeSem;
        this.personArrivedToPlaceSem = personArrivedToPlaceSem;
        this.personArrivedToStationSem = personArrivedToStationSem;
        this.personFinishedMeetingSem = personFinishedMeetingSem;
        this.accessToCarSem = accessToCarSem;
        //kolejki
        this.peopleInPlaces = peopleInPlaces;
        this.peopleQueue = peopleQueue;
        //grafika
        this.carImage = carImage;
    }

    @Override
    public void run() {
        while (true){
            try{
                accessToPlaceSem.acquire();
                int index = getIndexOfPlaceWherePersonFinishedMeeting();
                System.out.println(this + " - index: " + index);

                //dwie opcje:
                //1.nie ma osoby czekajacej po spotkaniu; samochod zabierze osobe z zajezdni
                //2.jakas osoba czeka po spotkaniu w jednym z miejsc spotkan; samochod pojedzie po osobe

                //**************************************OPCJA1*****************************************************
                if (index == -1){
                    accessToPlaceSem.release();
                    personSem.acquire();
                    accessToPeopleSem.acquire();

                    if (noOfDrivingCars < allCars){ //czy jest wolny samochod?


                        accessToCarSem.acquire();
                        noOfDrivingCars++;
                        accessToCarSem.release();

                        passenger = peopleQueue.remove(getIndexOfPrMinisterOrPresident());
                        accessToPeopleSem.release();
                        driverInvitedToCarSem[passenger.getIdP() - 1].release();
                        //System.out.println(this + " - pobieram osobe: " + passenger);
                        waitForPersonGoingToCar
                                (passenger.getDurationOfMovingPersonFromQueueToBorParking() + 2000 + 100);

                        int drawnPlace = passenger.getDrawnPlace();

                        //System.out.println(this + " - drawnPlace: " + drawnPlace);
                        accessToCarSem.acquire();
                        moveCarToWaitingPlaceForCars(drawnPlace);
                        accessToCarSem.release();
                        waitForEndOfDrivingToWaitingPlaceForCars();

                        placeSem[drawnPlace].acquire();
                        //System.out.println(this + " uzyskalem dostep do miejsca: " + drawnPlace);
                        moveCarToPlaceParking(drawnPlace);

                        personArrivedToPlaceSem[passenger.getIdP()-1].release();

                        if (prMinisterOrPresidentDoesNotHaveFreeCar()) {
                            //System.out.println(this + " musze wracac do bazy po premiera lub prezydenta");
                            accessToPlaceSem.acquire();
                            peopleInPlaces.put(drawnPlace, passenger);
                            accessToPlaceSem.release();

                            accessToCarSem.acquire();
                            moveCarToBorParking();
                            noOfDrivingCars--;
                            accessToCarSem.release();
                            waitForEndOfDrivingToBorParking();
                        } else {
                            //System.out.println(this + " - czekam na osobe na parkingu");
                            personFinishedMeetingSem[passenger.getIdP() - 1].acquire();
                            passenger.getPersonImage().setVisible(false);
                            //System.out.println(this + " - osoba: " + passenger + " skonczyla meeting. Widocznosc na false");
                            placeSem[drawnPlace].release();

                            accessToCarSem.acquire();
                            moveCarToBorParking();
                            noOfDrivingCars--;
                            accessToCarSem.release();
                            waitForEndOfDrivingToBorParking();
                            personArrivedToStationSem[passenger.getIdP() - 1].release();
                            //System.out.println(this + " personArrivedToStationSem dla osoby: " + passenger);
                        }
                    } else{
                        accessToPeopleSem.release();
                        personSem.release();
                    }
                }
                //*************************************OPCJA2*****************************************************/
                //na samochod czeka osoba po spotkaniu
                else {
                    if (noOfDrivingCars < allCars){ //czy jest wolny samochod
                        //System.out.println(this + " OPCJA2");
                        accessToCarSem.acquire();
                        noOfDrivingCars++;
                        moveCarToWaitingPlaceForCars(index);
                        accessToCarSem.release();
                        waitForEndOfDrivingToWaitingPlaceForCars();

                        passenger = peopleInPlaces.remove(index);
                        accessToPlaceSem.release();
                        //System.out.println(this + ", " + peopleInPlaces.size());
                        personFinishedMeetingSem[passenger.getIdP() - 1].acquire();
                        passenger.getPersonImage().setVisible(false);
                        System.out.println(this + " - pobieram z miejsca: " + index + " osobe: " + passenger +
                                " i ustawiam widocznosc na false");

                        placeSem[index].release();

                        accessToCarSem.acquire();
                        moveCarToBorParking();
                        noOfDrivingCars--;
                        accessToCarSem.release();
                        waitForEndOfDrivingToBorParking();

                        personArrivedToStationSem[passenger.getIdP() - 1].release();
                        //System.out.println(this + " personArrivedToStationSem dla osoby: " + passenger);
                    }
                }

            } catch (InterruptedException e){
                e.printStackTrace();
            }}
    }


    //                                      2.grafika - ruch

    /**method sets transition and play it
     * @param element element to move
     * @param toX Specifies the stop X coordinate value of moving
     * @param toY Specifies the stop X coordinate value of moving
     * @param duration The duration of moving
     * @return Translate transition
     */
    private TranslateTransition moveElement (Node element, double toX, double toY,
                                             Duration duration){
        TranslateTransition transition = new TranslateTransition(duration);
        transition.setNode(element);

        transition.setToX(toX);
        transition.setToY(toY);
        Platform.runLater(transition::play);

        return transition;
    }

    /**
     * method calculating coordinate X and coordinate Y of car in waiting place for cars and move car to this place
     * @param placeIndex index Of meeting place (0-allPlaces) drawn by person
     */
    private void moveCarToWaitingPlaceForCars (int placeIndex){
        double fromCarToZeroPositionX = 0 - carImage.getLayoutX();
        double fromCarToZeroPositionY = 0 - carImage.getLayoutY();

        double carPositionInWaitingPlaceForCarsX = places[placeIndex].getWaitingPlaceForCarsX();
        double carPositionInWaitingPlaceForCarsY = places[placeIndex].getWaitingPlaceForCarsY();

        moveElement(carImage, fromCarToZeroPositionX + carPositionInWaitingPlaceForCarsX,
                fromCarToZeroPositionY + carPositionInWaitingPlaceForCarsY,
                Duration.millis(durationOfMovingCarToWaitingPlaceForCars)).setOnFinished(event -> {
            carImage.setLayoutX(carPositionInWaitingPlaceForCarsX);
            carImage.setLayoutY(carPositionInWaitingPlaceForCarsY);
            carImage.setTranslateX(0);
            carImage.setTranslateY(0);
        });
    }

    /**
     * method calculating coordinate X and coordinate Y of car in meeting place parking and move car to this place
     * @param placeIndex index Of meeting place (0-allPlaces) drawn by person
     */
    private void moveCarToPlaceParking (int placeIndex){
        final double carPositionInPlaceParkingX = places[placeIndex].getPlaceParkingRectangle().getLayoutX() + 5;
        final double carPositionInPlaceParkingY = carImage.getLayoutY();

        TranslateTransition transition = new TranslateTransition(Duration.millis(2000));
        transition.setNode(carImage);

        transition.setToX(carPositionInPlaceParkingX - carImage.getLayoutX());
        transition.setToY(0);
        Platform.runLater(transition::play);


        System.out.println(this + " - bede sie przesuwal na parking miejsca");
        System.out.println("wspolrzedna X: " + (carPositionInPlaceParkingX - carImage.getLayoutX()));

        transition.setOnFinished(event -> {
            carImage.setTranslateX(0);
            carImage.setTranslateY(0);
            carImage.setLayoutX(carPositionInPlaceParkingX);
            carImage.setLayoutY(carPositionInPlaceParkingY);
            System.out.println(this + " - skonczylem sie przesuwac na parking miejsca");
        });

        try {
            sleep(2000 + 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param imageView image to flip
     * @param durationInMillis duration in milliseconds of flipping image
     * @return ScaleTransition
     */
    private ScaleTransition flipImage (ImageView imageView, double durationInMillis){
        Duration halfOfDuration = new Duration(durationInMillis/2);
        ScaleTransition front = new ScaleTransition(halfOfDuration, imageView);
        front.setFromX(1);
        front.setToX(0);

        ScaleTransition back = new ScaleTransition(halfOfDuration, imageView);
        back.setFromX(0);
        back.setToX(1);

        front.play();

        front.setOnFinished(event -> {
            imageView.setScaleX(0);
            imageView.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
            back.play();
        });
        return back;
    }

    private void moveCarToBorParking (){
        double flipImageDuration = 0.3 * durationOfMovingCarToBorParking;
        double moveCarDuration = durationOfMovingCarToBorParking - flipImageDuration;

        double fromPersonToZeroPositionX = 0 - carImage.getLayoutX();
        double fromPersonToZeroPositionY = 0 - carImage.getLayoutY();

        flipImage(carImage, flipImageDuration).setOnFinished(event -> {
            moveElement(carImage,fromPersonToZeroPositionX + inBorParkingCarPositionX,
                    fromPersonToZeroPositionY + inBorParkingCarPositionY,
                    Duration.millis(moveCarDuration)).setOnFinished(event1 -> {
                carImage.setLayoutX(inBorParkingCarPositionX);
                carImage.setLayoutY(inBorParkingCarPositionY);
                carImage.setTranslateX(0);
                carImage.setTranslateY(0);
                carImage.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            });
        });
    }


    //                                      3.logika

    @Override
    public String toString() {
        return "Car" + idC;
    }

    /**
     * @param from the lower limit of time to draw given in seconds
     * @param to the upper limit of time to draw given in seconds
     */
    private int drawTimeToWait (int from, int to){
        //zamiana na milisekundy
        from *= 1000;
        to *= 1000;

        Random random = new Random();
        return random.nextInt(to-from) + from;
    }

    /**
     * @return index of place where person finished meeting or -1 if there are not place where person finished meeting
     */
    private int getIndexOfPlaceWherePersonFinishedMeeting() {
        int index = -1;

        //pobranie numeru miejsca i osoby
        for(Map.Entry<Integer, Person> entry : peopleInPlaces.entrySet()) {
            Integer key = entry.getKey();
            Person value = entry.getValue();
            if (value.isFinishedMeeting()){
                index = key;
                break;
            }
        }
        return index;
    }

    /**method put thread to sleep to wait for going person to car
     * @param durationInMillis duration in milliseconds of waiting for going person to car
     */
    private void waitForPersonGoingToCar (int durationInMillis){
        try {
            sleep(durationInMillis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @return index from peopleQueue of Prime Minister or President or
     * if there aren't return 0 (first element in queue)
     */
    private int getIndexOfPrMinisterOrPresident() {
        for (int i = 0; i<peopleQueue.size(); i++){
            if (peopleQueue.get(i).isThisPremier() || peopleQueue.get(i).isThisPrezydent()){
                return i;
            }
        }
        return 0;
    }

    /**
     * method put thread to sleep to wait for end of driving car to waiting place for cars
     */
    private void waitForEndOfDrivingToWaitingPlaceForCars(){
        try {
            sleep(durationOfMovingCarToWaitingPlaceForCars+100);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * looking for person which is Prime Minister or President, does not have free car and is waiting
     * for car in people queue
     * @return if Prime Minister or President wait in people queue and does not have free car
     */
    private boolean prMinisterOrPresidentDoesNotHaveFreeCar(){
        //na prezydenta i premiera samochod musi czekac na spotkaniu
        if (!passenger.isThisPremier() && !passenger.isThisPrezydent()){
            //jesli nie ma wolnego samochodu w zajezdni BOR
            if (noOfDrivingCars == allCars){
                for (Person personInDepot : peopleQueue){
                    if (personInDepot.isThisPremier() || personInDepot.isThisPrezydent()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * method put thread to sleep to wait for end of driving car to car (BOR) parking
     */
    private void waitForEndOfDrivingToBorParking(){
        try {
            sleep(durationOfMovingCarToBorParking + 100); //wylosowanie czasu jechania do stacji od 3 do 5 sekund
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}