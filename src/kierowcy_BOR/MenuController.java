package kierowcy_BOR;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.Node;


public class MenuController {
    @FXML
    Pane pane;

    @FXML
    void initialize () {
        ChoiceBox<Integer> carsAmountChoice = new ChoiceBox<>();
        carsAmountChoice.setLayoutX(739);
        carsAmountChoice.setLayoutY(311);
        carsAmountChoice.setValue(2);

        for (int i = 2; i < 15; i++) {
            carsAmountChoice.getItems().add(i);
        }

        ChoiceBox<Integer> peopleAmountChoice = new ChoiceBox<>();
        peopleAmountChoice.setLayoutX(739);
        peopleAmountChoice.setLayoutY(389);
        peopleAmountChoice.setValue(3);
        for (int i = 1; i < 26; i++) {
            peopleAmountChoice.getItems().add(i);
        }

        Button runSimulation = new Button("Uruchom symulację");
        runSimulation.setLayoutX(1022);
        runSimulation.setLayoutY(557);

        pane.getChildren().addAll(carsAmountChoice, peopleAmountChoice, runSimulation);

        runSimulation.setOnAction(event -> {
            Integer allCars = carsAmountChoice.getValue();
            Integer allPeople = peopleAmountChoice.getValue();

            System.out.println("allCars: " + allCars);
            System.out.println("allPeople: " + allPeople);

            SimulationController simulationController = new SimulationController();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("simulation.fxml"));
            loader.setController(simulationController);
            Pane pane2 = null;
            try {
                pane2 = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Scene simulationScene = new Scene(pane2);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(simulationScene);
            stage.show();
            simulationController.startSimulation(allCars, allPeople);

        });
    }
}
