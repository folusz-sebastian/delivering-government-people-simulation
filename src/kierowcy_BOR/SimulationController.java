package kierowcy_BOR;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.*;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SimulationController {
    private final int allPlaces = 5;

    @FXML
    private
    Pane pane;

    @FXML
    private
    Rectangle borParkingRectangle;

    @FXML
    private
    Rectangle placeParkingRectangle1;
    @FXML
    private
    Rectangle placeParkingRectangle2;
    @FXML
    private
    Rectangle placeParkingRectangle3;
    @FXML
    private
    Rectangle placeParkingRectangle4;
    @FXML
    private
    Rectangle placeParkingRectangle5;

    @FXML
    private
    Group place1;
    @FXML
    private
    Group place2;
    @FXML
    private
    Group place3;
    @FXML
    private
    Group place4;
    @FXML
    private
    Group place5;

    @FXML
    private
    ImageView stationImage;

    void startSimulation(int allCars, int allPeople) {
        //semafory
        Semaphore personSem = new Semaphore(0);
        Semaphore accessToCar = new Semaphore(1);
        Semaphore accessToPlaceSem = new Semaphore(1);
        Semaphore accessToPeopleSem = new Semaphore(1);
        Semaphore placeSem [] = getSemaphores(allPlaces, 1);
        Semaphore personArrivedToPlaceSem [] = getSemaphores(allPeople, 0);
        Semaphore driverInvitedToCarSem [] = getSemaphores(allPeople, 0);
        Semaphore personArrivedToStationSem [] = getSemaphores(allPeople, 0);
        Semaphore personFinishedMeeting [] = getSemaphores(allPeople, 0);

        //kolejki
        Map<Integer, Person> peopleInPlaces = new HashMap<>();
        List<Person> peopleQueue = new ArrayList<>();	        //mozliwe wyciaganie elementow ze srodka
        List<Group> peopleImagesQueue = new LinkedList<>();

        //graficzne elementy miejsc spotkań
        Group placeImage [] = {place1, place2, place3, place4, place5};
        Rectangle placeParkingRectangle [] = {placeParkingRectangle1, placeParkingRectangle2, placeParkingRectangle3,
                placeParkingRectangle4, placeParkingRectangle5};

        //graficzne elementy samochodow
        ImageView[] carImage = getImageView(allCars, "/images/car/car-image.jpg", 30.0, 85.0);
        for (int i = 0; i < carImage.length; i++) {
            setCarLayout(carImage[i], i);
        }

        //graficzne elementy osob
        ImageView[] personImage = getImageView(allPeople, "/images/people/worker.gif", 33.0, 12.0);
        Rectangle[] personMark = getPersonMark(allPeople);
        Group personWithMark [] = getPersonWithMark(allPeople, personImage, personMark);

        //dodanie graficznych elementow
        pane.getChildren().addAll(carImage);
        pane.getChildren().addAll(personWithMark);

        //tworze obiekty miejsc
        Place places[] = new  Place[allPlaces];
        for (int i = 0; i < places.length; i++) {
            places[i] = new Place(placeImage[i], placeParkingRectangle[i]);
        }

        //tworze obiekty osob
        Person people [] = new Person[allPeople];
        for (int i = 0; i < people.length; i++) {
            people[i] = new Person(allPlaces, places, personSem, personArrivedToPlaceSem, personArrivedToStationSem,
                    accessToPeopleSem, personFinishedMeeting, peopleQueue, stationImage, personWithMark[i],
                    personMark[i], borParkingRectangle, driverInvitedToCarSem, peopleImagesQueue);
        }

        //tworze obiekty samochodow
        Car cars [] = new Car[allCars];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = new Car(places, allCars, accessToCar, accessToPlaceSem, accessToPeopleSem, personSem, placeSem,
                    personArrivedToPlaceSem, personArrivedToStationSem, personFinishedMeeting, driverInvitedToCarSem, peopleInPlaces,
                    peopleQueue, carImage[i]);
        }

        //tworze watki
        int max = Math.max(people.length, cars.length);

        for (int i = 0; i < max; i++) {
            if (i<people.length) people[i].start();
            if (i<cars.length) cars[i].start();
        }
    }

    private Semaphore[] getSemaphores(int size, int permits) {
        Semaphore semaphoreTab[] = new Semaphore[size];
        for (int i = 0; i < semaphoreTab.length; i++) {
            semaphoreTab[i] = new Semaphore(permits);
        }
        return semaphoreTab;
    }

    private ImageView[] getImageView(int noOfImages, String filePath, double fitHeight, double fitWidth) {
        Image imagesTab[] = new Image[noOfImages];
        for (int i = 0; i < imagesTab.length; i++) {
            imagesTab[i] = new Image(filePath);
        }

        ImageView imageViews[] = new ImageView[noOfImages];
        for (int i = 0; i < imageViews.length; i++) {
            imageViews[i] = new ImageView();
            imageViews[i].setFitHeight(fitHeight);
            imageViews[i].setFitWidth(fitWidth);
            imageViews[i].setImage(imagesTab[i]);
        }
        return imageViews;
    }

    private Rectangle[] getPersonMark(int allPeople) {
        Rectangle personMark [] = new Rectangle[allPeople];
        for (int i = 0; i < personMark.length; i++) {
            personMark[i] = new Rectangle(14, 34);
            personMark[i].setFill(Paint.valueOf("#f5f5f5"));
            personMark[i].setStroke(Paint.valueOf("#c02323"));
            personMark[i].setStrokeWidth(0);
        }
        return personMark;
    }

    private Group[] getPersonWithMark(int allPeople, ImageView person [], Rectangle personMark[]) {
        Group personWithMark [] = new Group[allPeople];
        for (int i = 0; i < personWithMark.length; i++) {
            personWithMark[i] = new Group(personMark[i], person[i]);
            personWithMark[i].setVisible(false);
        }
        return personWithMark;
    }

    private void setCarLayout (ImageView car, int whichCar){
        final double startCarsPositionX = borParkingRectangle.getLayoutX() + 10;
        final double startCarsPositionY = borParkingRectangle.getLayoutY() + 10;
        final int maxCarsInColumn = 10;
        final int distanceBetweenTwoElementsX = 105;
        final int distanceBetweenTwoElementsY = 40;

        car.setLayoutX(((whichCar)/maxCarsInColumn)*distanceBetweenTwoElementsX + startCarsPositionX);
        car.setLayoutY((whichCar%10)*distanceBetweenTwoElementsY + startCarsPositionY);
    }
}