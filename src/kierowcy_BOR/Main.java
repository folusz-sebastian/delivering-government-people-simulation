package kierowcy_BOR;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        MenuController menuController = new MenuController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("menu.fxml"));
        loader.setController(menuController);
        Pane pane = loader.load();
        primaryStage.setTitle("Symulacja działania BOR");
        primaryStage.setScene(new Scene(pane, 1280, 660));
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
