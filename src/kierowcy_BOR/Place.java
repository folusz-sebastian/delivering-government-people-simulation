package kierowcy_BOR;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;


public class Place{
    /**
     * place counter which count all objects of place
     */
    private static int placeCounter = 0;
    /**
     * id number of place
     */
    private int idPl;

    //grafika
    private Group placeImage;
    private Rectangle placeParkingRectangle;

    /**
     * @param placeImage graphic element (Group) which contains image and label of meeting place
     * @param placeParkingRectangle graphic element (Rectangle) of car parking
     */
    public Place(Group placeImage, Rectangle placeParkingRectangle) {
        placeCounter++;
        idPl = placeCounter;
        //grafika
        this.placeImage = placeImage;
        this.placeParkingRectangle = placeParkingRectangle;
    }


    //                                      1.grafika - wspolrzedne

    /**
     * @return coordinate X of entry to meeting place
     */
    public double getPlaceEntryX (){
        return placeImage.getLayoutX() + placeImage.getBoundsInParent().getWidth()/2;
    }

    /**
     * @return coordinate Y of entry to meeting place
     */
    public double getPlaceEntryY (){
        return placeImage.getLayoutY() + placeImage.getBoundsInParent().getHeight() - 60.0;
    }

    /**
     * @return coordinate X of getting out of the car in meeting place
     */
    public double getGetoutOfTheCarInPlaceX (){
        return placeParkingRectangle.getLayoutX() + placeParkingRectangle.getWidth()/2 - 6;
    }

    /**
     * @return coordinate Y of getting out of the car in meeting place
     */
    public double getGetoutOfTheCarInPlaceY(){
        return getPlaceEntryY();
    }

    /**
     * @return coordinate X of place where car is waiting for release meeting place parking
     */
    public double getWaitingPlaceForCarsX (){
        return 946.0;
    }

    /**
     * @return coordinate Y of place where car is waiting for release meeting place parking
     */
    public double getWaitingPlaceForCarsY (){
        return (idPl-1)*133 + 67;
    }



    //                                      2.grafika - elementy - gettery

    /**
     * @return Rectangle which is car parking
     */
    public Rectangle getPlaceParkingRectangle() {
        return placeParkingRectangle;
    }


    //                                      3.pozostałe

    @Override
    public String toString() {
        return "Place" + idPl;
    }
}
